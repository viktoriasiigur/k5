import java.lang.reflect.Array;
import java.util.*;

public class Tnode {

    private String name;
    private Tnode firstChild;
    private Tnode nextSibling;

    Tnode(String s, Tnode p, Tnode a) {
        setName(s);
        setRightSibling(p);
        setFirstChild(a);
    }

    public void setName(String s) {
        name = s;
    }

    public String getName() {
        return name;
    }

    public void setRightSibling(Tnode p) {
        nextSibling = p;
    }

    public Tnode getRightSibling() {
        return nextSibling;
    }

    public void setFirstChild(Tnode a) {
        firstChild = a;
    }

    public Tnode getFirstChild() {
        return firstChild;
    }

    public void processTreeNode() {
        System.out.print(getName() + "  ");
    }

    public boolean hasNext() {
        return (getRightSibling() != null);
    }

    public Tnode next() {
        return getRightSibling();
    }

    public Tnode children() {
        return getFirstChild();
    }

    public void preOrder() {
        processTreeNode();
        Tnode children = children();
        while (children != null) {
            children.preOrder();
            children = children.next();
        }
    }

    public void addChild(Tnode a) {
        if (a == null) return;
        Tnode children = children();
        if (children == null)
            setFirstChild(a);
        else {
            while (children.hasNext())
                children = (children.next());
            children.setRightSibling(a);
        }
    }

    public boolean isLeaf() {
        return (getFirstChild() == null);
    }

    public int size() {
        int n = 1; // root
        Tnode children = children();
        while (children != null) {
            n = n + children.size();
            children = children.next();
        }
        return n;
    }

    @Override
    public String toString() {
        StringBuffer b = new StringBuffer();
        b.append(this.getName());
        if (!this.isLeaf()) {
            b.append("(");
            b.append(this.getFirstChild().toString());
            b.append(")");
        }
        if (this.hasNext()) {
            b.append(",");
            b.append(this.getRightSibling().toString());
        }
        return b.toString();
    }


    public static Tnode buildFromRPN(String pol) {
        String[] operations = {"/", "*", "+", "-"};
        StringTokenizer splittedPol = new StringTokenizer(pol);
        Stack<Tnode> st = new Stack<>();
        Tnode t;
        Tnode t1;
        Tnode t2;
        try{
            while (splittedPol.hasMoreTokens()) {
                String token = splittedPol.nextToken();
                if (token.matches("[-]?\\d+")) {
                    t = new Tnode(token, null, null);
                    st.push(t);
                } else if (Arrays.asList(operations).contains(token)){
                    t = new Tnode(token, null, null);
                    t1 = st.pop();
                    t2 = st.pop();
                    t.setFirstChild(t2);
                    t.addChild(t1);
                    st.push(t);
                } else if (token.equals("ROT")) {
                    try{
                        Tnode el3 = st.pop();
                        Tnode el2 = st.pop();
                        Tnode el1 = st.pop();
                        st.push(el2);
                        st.push(el3);
                        st.push(el1);
                    } catch (EmptyStackException e) {
                        throw new RuntimeException(pol + ": unable to do operation 'ROT'");
                    }
                } else if (token.equals("SWAP")) {
                    try{
                        Tnode el2 = st.pop();
                        Tnode el1 = st.pop();
                        st.push(el2);
                        st.push(el1);
                    } catch (EmptyStackException e) {
                        throw new RuntimeException(pol + ": cannot do operation 'SWAP'");
                    }
                }
                else {
                    throw new RuntimeException(pol + ": contains illegal symbol");
                }
            }
            t = st.peek();
            st.pop();
        } catch (EmptyStackException e) {
            throw new RuntimeException(pol + ": wrong amount of numbers");
        }

        if (!st.empty()) throw new RuntimeException(pol + ": too many numbers");
        return t;
    }

    public static void main(String[] param) {
        String rpn = "5 SWAP -";
        System.out.println("RPN: " + rpn);
        Tnode res = buildFromRPN(rpn);
        System.out.println("Tree: " + res.toString());
        System.out.println("Number of nodes: " + (res.size()));
    }
}

// https://enos.itcollege.ee/~jpoial/algoritmid/puud.html
// https://www.geeksforgeeks.org/expression-tree/
// https://git.wut.ee/i231/home5/raw/branch/master/src/Node.java

